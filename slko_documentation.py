#!/bin/python3.3

''' slko_documentation.py - A program to document DFTB Slater-Koster files
                            with respect to the dftb.org slko package
                            documentation guidelines.
                            Learn more about DFTB at http://www.dftb.org/

    Maximilian Kubillus @ Febuary 2015, maximilian.kubillus@gmail.com
'''

###############################################################################
##
## ToDo's and ideas for possible improvements:
## - Implement HSD Parser or something similar that works well
## - Complete parser for slateratom inputs
## - Validity checks for slateratom inputs
## - Make atom folders for electronics case insensitive
## - Improve usability (more verbous and more in numbers)
## - Remove chunks of unnecessary code (add_atom method etc.)
## - Export classes etc. into external library (doesn't improve usability...)
## - Conform with some style guide (this is semi-conform to Google's guide)
##
###############################################################################

import argparse
import datetime
import os
import re
import shutil
import sys
import textwrap


## Classes and methods

class InputData:
    '''Contains parsed and sorted input data from the input file to use in
    instantiation of the separate documentation classes.'''

    def __init__(self, input_file):
        self.input_file = input_file
        self.parse_one_line_input()
        self.parse_region_input()
        self.parse_atom_data()
        try:
            self.parse_one_line_input()
            self.parse_region_input()
            self.parse_atom_data()
        except:
            sys.exit('Error while parsing input data.')
        try:
            self.general = General(self.author, self.date, self.atoms)
            self.sk_table = SKTable(self.code, self.funct, self.suppos)
            for atom in self.atom_data.keys():
                self.sk_table.add_atom(atom, self.atom_data[atom])
            self.e_rep = ERep(self.storage, self.scc, self.thirdorder,
                              self.abinitio, self.fitsystems)
            self.misc = Misc(self.accuracy, self.failures, self.publication,
                             self.notes)
        except:
            sys.exit('Error while initiating documentation objects.')


    def parse_one_line_input(self):
        '''Parses all one-line input file parameters for the program.'''

        self.out_dir = self.req_input('OutputDir')
        self.out_dir = self.out_dir.rstrip('/ \n')
        self.elec_dir = self.req_input('ElectronicsDir')
        self.elec_dir = self.elec_dir.rstrip('/ \n')
        self.slko_dir = self.req_input('SlakoDir')
        self.author = self.req_input('Author')
        self.code = self.req_input('TwoCenterCode')
        self.suppos = self.req_input('Superposition')
        self.storage = self.req_input('ERepStorage')
        self.scc = self.req_input('SCC')
        self.thirdorder = self.req_input('ThirdOrder')
        self.atoms = self.req_input('Atoms').split(' ')
        ## Capitalize each atom name for output.
        self.atoms = [atom.capitalize() for atom in self.atoms]
        regexpr = re.compile(r'^HubbDerivs\s*=\s*\'(.*?)\'$', re.I)
        match = find_regexpr(self.input_file, regexpr)
        if (match):
            self.u_der = match.group(1).split(' ')
        else:
            self.u_der = [''] * 10
        ## Parse date if given or insert today's date.
        regexpr = re.compile(r'^Date\s*=\s*\'(.*?)\'$', re.I)
        match = find_regexpr(self.input_file, regexpr)
        if (match):
            self.date = match.group(1)
        else:
            self.date = datetime.date.today()
        regexpr = re.compile(r'^DFTFunctional\s*=\s*\'(.*?)\'$', re.I)
        match = find_regexpr(self.input_file, regexpr)
        if (match):
            self.funct = match.group(1)
        else:
            self.funct = None
        return True


    def req_input(self, option):
        '''Checks wether a required input option is given (with regex) and
        returns it. In case of missing input the program will be halted.'''
        regexpr = re.compile(r'^%s\s*=\s*\'(.*?)\'$' % option, re.I)
        try:
            tmpInp = find_regexpr(self.input_file, regexpr).group(1)
        except AttributeError:
            sys.exit('Required input option %s not found. Aborting...' %
                     option)
        return tmpInp

    def parse_region_input(self):
        '''Parses all region input file parameters for the program.'''

        regexpr = re.compile(r'^AbInitio\s*=\s*.*$', re.I)
        self.abinitio = find_regexpr_region(self.input_file, regexpr)
        regexpr = re.compile(r'^FitSystems\s*=\s*.*$', re.I)
        self.fitsystems = find_regexpr_region(self.input_file, regexpr)
        regexpr = re.compile(r'^Accuracy\s*=\s*.*$', re.I)
        self.accuracy = find_regexpr_region(self.input_file, regexpr)
        regexpr = re.compile(r'^Failures\s*=\s*.*$', re.I)
        self.failures = find_regexpr_region(self.input_file, regexpr)
        regexpr = re.compile(r'^Publication\s*=\s*.*$', re.I)
        self.publication = find_regexpr_region(self.input_file, regexpr)
        regexpr = re.compile(r'^MiscNotes\s*=\s*.*$', re.I)
        self.notes = find_regexpr_region(self.input_file, regexpr)


    def parse_atom_data(self):
        '''Parses electronic parameters from given input files in given
        electronics directory and sets up the atoms dictionary.

        The filestructure of the electronic parameter directory has to be:
        one folder named with lowercase symbol for each as given in input
        file and for each atom folder two directories "dens" and "wave"
        (for density and wavefunction generation) with slateratom input
        files called "STDIN".'''

        self.atom_data = {} # Dictionary containing all atoms with data on exit.
        iAtom = 0
        while iAtom < len(self.atoms):
            at = {} # Temporary atom data dictionary
            atom_dir = '%s/%s' % (self.elec_dir, self.atoms[iAtom].lower())
            wave_file = parse_slateratom_input('%s/wave/STDIN' % atom_dir)
            dens_file = parse_slateratom_input('%s/dens/STDIN' % atom_dir)
            at['at_id'] = self.atoms[iAtom] # Atom symbol
            l_max = int(wave_file[0][1])
            if (wave_file[0][3] == '.true.'):
                at['relativistic'] = 'ZORA'
            elif (wave_file[0][3] == '.false.'):
                at['relativistic'] = 'no'
            else:
                at['relativistic'] = 'Unknown'
            at['max_ang'] = l_max # Maximum angular momentum
            at['funct'] = int(wave_file[1][0]) # Functional integer
            ii = 0
            r_wf = []
            while ii <= l_max:
                r_wf.append(wave_file[2+ii][0])
                ii += 1
            at['r_wf'] = ' '.join(r_wf) # Wave function confinement radii
            at['r_dens'] = dens_file[2][0] # Density confinement
            at['sigma'] = float(wave_file[2][1]) # Conf. potential exponent
            n_exp = int(wave_file[2+2*(l_max+1)][0]) # No. of alpha exponents
            aa = []
            if (wave_file[2+3*(l_max+1)][0] == '.true.'):
                aa.append(float(wave_file[3+3*(l_max+1)][0]))
                aa_max = float(wave_file[3+3*(l_max+1)][1])
                ii = 1
                while ii < 4:
                    aa.append(aa[0] * (aa_max / aa[0])**(float(ii)/n_exp))
                    ii += 1
                aa.append(aa_max)
                aa = [str(round(entry, 2)) for entry in aa]
            else:
                ii = 0
                while ii < n_exp:
                    aa.append(wave_file[3+3*(l_max+1)+ii][0])
                    ii += 1
            at['aa'] = aa # Alpha exponents
            at['polynom'] = wave_file[2+2*(l_max+1)][1] # Order of polynomial
            at['u_der'] = self.u_der[iAtom] # Hubbard derivative
            at['n_occ'] = [line[1] for line in wave_file[-(l_max+1):]]
            atom = Atom(at)
            self.atom_data[self.atoms[iAtom]] = atom
            iAtom += 1


class Atom:
    '''Contains all needed data for describing one atom documentation.'''

    def __init__(self, at_input):
        '''Basic instantiation routine of the class. The atom input needs to be
        a dictionary with the following keys: at_id (str), aa (list),
        n_occ (list), max_ang (int), funct (str), r_wf (float), r_dens (float),
        sigma (float), polynom (int), u_der (float), relativistic (str)'''
        if (len(at_input['n_occ']) != at_input['max_ang'] + 1):
            print('Error: Maximum angular momentum and occupied shells' \
                  'incompatible!')
            return None
        self.at_id = at_input['at_id']
        self.aa = at_input['aa']
        self.n_occ = at_input['n_occ']
        self.max_ang = at_input['max_ang']
        self.funct_int = at_input['funct']
        self.r_wf = at_input['r_wf']
        self.r_dens = at_input['r_dens']
        self.sigma = at_input['sigma']
        self.polynom = at_input['polynom']
        self.u_der = at_input['u_der']
        self.relativistic = at_input['relativistic']


    def funct(self):
        '''Returns the correct functional string for the atom documentation.'''
        if (self.funct_int == 0):
            functional = 'Hartree-Fock'
        elif (self.funct_int == 1):
            functional = 'X-Alpha'
        elif (self.funct_int == 2):
            functional = 'PW-LDA'
        elif (self.funct_int == 3):
            functional = 'PBE'
        else:
            functional = 'Unknown Type'
        return functional


    def shells(self):
        '''Returns the correct shells string for the atom documentation.'''
        shells = []
        ii = 0
        while ii <= self.max_ang:
            if (ii == 0):
                shells.append('%ss' % self.n_occ[ii])
            elif(ii == 1):
                shells.append('%sp' % self.n_occ[ii])
            elif(ii == 2):
                shells.append('%sd' % self.n_occ[ii])
            elif(ii == 3):
                shells.append('%sf' % self.n_occ[ii])
            ii += 1
        return ' '.join(shells)

    def exponents(self):
        '''Returns a string with all alpha values for the atom documentation.'''
        return ' '.join(self.aa)


class General:
    '''Contains all needed data for the slater-koster file documentation
    general section.'''

    def __init__(self, author: str, date: str, atoms: list):
        '''Basic instantiation routine of the class.'''
        self.author = author
        self.date = date
        self.atoms = atoms
        self.filename = ''
        self.at1 = ''
        self.at2 = ''


    def update_pair(self, at1: str, at2: str):
        '''Updates the currently used atom pair combination.'''
        filename = "%s-%s.skf" % (at1.capitalize(), at2.capitalize())
        try:
            self.at1 = at1
            self.at2 = at2
            self.filename = filename
            return True
        except:
            return False


class SKTable:
    '''Container for all needed data to document electronic table data.'''

    def __init__(self, twocnt_code: str, funct: str, suppos: str):
        '''Basic instantiation routine of the class.'''
        self.code = twocnt_code
        self.funct = funct
        self.suppos = suppos
        self.atom_data = {}
        self.at1 = {}
        self.at2 = {}


    def add_atom(self, atom: str, data):
        '''Adds an atom to the dictionary of atoms. The variable atom should be
        a dictionary that contains keys for r_dens, r_wf, shells, exponents,
        power, sigma, and u_der with respective values.'''
        try:
            self.atom_data[atom] = data
            return True
        except:
            print('Adding atom dictionary to SKTable failed!')
            return False


    def update_pair(self, at1: str, at2: str):
        '''Updates the currently used atom pair combination.'''
        try:
            self.at1 = self.atom_data[at1]
            self.at2 = self.atom_data[at2]
            return True
        except:
            print('Updating atom pair to SKTable failed!')
            return False


class ERep:
    '''Container for all needed data to document repulsive spline data. It is
    intended to only supply minimal data and direct to its respective
    publication, for individual repulsive potential data the class and its
    methods have to be extended.'''

    def __init__(self, storage: str, scc: str, thirdorder: str, abinitio: list,
                 fitsystems: list):
        '''Basic instantiation routine of the class.'''
        self.storage = storage
        self.scc = scc
        self.thirdorder = thirdorder
        self.abinitio = abinitio
        self.fitsystems = fitsystems


class Misc:
    '''Container for all needed data to document miscellaneous data for the
    Slater-Koster files, including information about accuracy and mention of
    failures, related publication and notes regarding the given data.'''

    def __init__(self, accuracy: list, failures: list, publication: list,
                notes: list):
        '''Basic instantiation routine of the class.'''
        self.accuracy = accuracy
        self.failures = failures
        self.publication = publication
        self.notes = notes


## Start of global functions.

def parse_slateratom_input(file_path):
    '''Parses a slateratom input file, removes comments and returns each
    line as a list of strings (one entry for each word), stripped from all
    falsy elements.'''
    in_file = open(file_path, 'r').readlines()
    in_file = [line[:line.find('!')] for line in in_file]
    in_file = [line.strip().split() for line in in_file]
    in_file = [x for x in in_file if x]
    return in_file


def find_regexpr(search_file: list, regexpr):
    '''Looks for the given compiled regular expression in a list of strings and
    returns the match of the expression or None if no match is found.'''

    for line in search_file:
        match = regexpr.match(line)
        if (match):
            break
        else:
            pass
    if (match):
        return match
    else:
        return None


def find_regexpr_region(search_file: list, regexpr):
    '''Looks for the given compiled regular expression in a list of strings,
    like find_regexpr and returns the match of the expression or None if no
    match is found. Additionally the regular expression is required to be
    enclosed in braces {} and match can be a multi-line region.'''

    region = []
    file_length = len(search_file)
    ii = 0
    ## Find starting line.
    while ii < file_length:
        line = search_file[ii]
        ii += 1
        match = regexpr.match(line)
        if (match):
            region.append(line[line.find('{')+1:])
            break
        else:
            pass
    if (not match): # No matches found in the whole file.
        return None
    else:
        pass
    ## Complete the input region.
    while ii < file_length:
        line = search_file[ii]
        ii += 1
        try:
            region.append(line[:line.index('}')])
            break
        except ValueError: # Critical error: Input is not parsable
            if (ii == file_length):
                sys.exit('Input Error: There are open braces without' \
                         'matching closing braces in the input file!' \
                         '\nError found in section:\n%s' % match.group(0))
            else:
                region.append(line)
    ## Remove leading and trailing whitespace and empty strings at beginning or
    ## end of the region list.
    if (region[0].lstrip().rstrip() == ''):
        del region[0]
    elif (region[-1].lstrip().rstrip() == ''):
        del region[-1]
    else:
        pass
    region = [line.lstrip().rstrip() for line in region]
    region = [x for x in region if x]
    return region


def get_doc(inputs, at1: str, at2: str, tHomo: bool):
    '''Prepares the complete documentation splice, containing one documentation
    line per entry element.'''

    ## Update objects with current atom pair
    inputs.general.update_pair(at1, at2)
    inputs.sk_table.update_pair(at1, at2)
    doc = []
    doc.append('<Documentation>')
    ## Get separate sections of the documentation and merge them.
    gen_doc = get_doc_general(inputs.general, tHomo)
    sk_doc = get_sk_table(inputs.sk_table, tHomo)
    rep_doc = get_rep_doc(inputs.e_rep)
    misc_doc = get_misc_doc(inputs.misc)
    doc = doc + gen_doc + sk_doc + rep_doc + misc_doc
    doc.append('</Documentation>')
    return doc


def get_doc_general(general, tHomo: bool):
    '''Generates the "General" section of the Slater-Koster file
    documentation.'''

    gen_doc = []
    gen_doc.append('  <General>')
    gen_doc.append('    <Identifier>%s</Identifier>' % general.filename)
    gen_doc.append('    <Author>%s</Author>' % general.author)
    gen_doc.append('    <Creation>%s</Creation>' % general.date)
    gen_doc.append('    <Element1>%s</Element1>' % general.at1)
    if (tHomo):
        pass
    else:
        gen_doc.append('    <Element2>%s</Element2>' % general.at2)
    gen_doc.append('    <Md5sum></Md5sum>')
    gen_doc.append('    <Svn_Id></Svn_Id>')
    gen_doc.append('    <Svn_URL></Svn_URL>')
    gen_doc.append('    <Compatibility>')
    ## Partner files subsection
    if (tHomo):
        for at2 in general.atoms:
            if (at2 == general.at1):
                continue
            else:
                pair_fname = '%s-%s.skf' % (general.at1, at2)
                gen_doc.append('      <Partner Identifier="%s">' % pair_fname)
                gen_doc.append('        <Element1>%s</Element1>' % general.at1)
                gen_doc.append('        <Element2>%s</Element2>' % at2)
                gen_doc.append('        <Md5sum></Md5sum>')
                gen_doc.append('        <SvnLoc></SvnLoc>')
                gen_doc.append('      </Partner>')
                pair_fname = '%s-%s.skf' % (at2, general.at1)
                gen_doc.append('      <Partner Identifier="%s">' % pair_fname)
                gen_doc.append('        <Element1>%s</Element1>' % at2)
                gen_doc.append('        <Element2>%s</Element2>' % general.at1)
                gen_doc.append('        <Md5sum></Md5sum>')
                gen_doc.append('        <SvnLoc></SvnLoc>')
                gen_doc.append('      </Partner>')
    else:
        pair_fname = '%s-%s.skf' % (general.at1, general.at1)
        gen_doc.append('      <Partner Identifier="%s">' % pair_fname)
        gen_doc.append('        <Element1>%s</Element1>' % general.at1)
        gen_doc.append('        <Md5sum></Md5sum>')
        gen_doc.append('        <SvnLoc></SvnLoc>')
        gen_doc.append('      </Partner>')
        pair_fname = '%s-%s.skf' % (general.at2, general.at2)
        gen_doc.append('      <Partner Identifier="%s">' % pair_fname)
        gen_doc.append('        <Element1>%s</Element1>' % general.at2)
        gen_doc.append('        <Md5sum></Md5sum>')
        gen_doc.append('        <SvnLoc></SvnLoc>')
        gen_doc.append('      </Partner>')
    gen_doc.append('    </Compatibility>')
    gen_doc.append('  </General>')
    return gen_doc


def get_sk_table(sk_table, tHomo: bool):
    '''Generates the Slater-Koster table section of the Slater-Koster file
    documentation.'''

    sk_doc = []
    sk_doc.append('  <SK_table>')
    sk_doc.append('    <Code>%s</Code>' % sk_table.code)
    if (sk_table.funct):
        sk_doc.append('    <Functional>%s</Functional>' % sk_table.funct)
    else:
        sk_doc.append('    <Functional>%s</Functional>' % sk_table.at1.funct())
    sk_doc.append('    <Superposition>%s</Superposition>' % sk_table.suppos)
    sk_doc.append('    <Basis atom="1">')
    sk_doc.append('      <Shells>%s</Shells>' % sk_table.at1.shells())
    sk_doc.append('      <Relativistic>%s</Relativistic>' %
                  sk_table.at1.relativistic)
    sk_doc.append('      <Exponents>%s</Exponents>' % sk_table.at1.exponents())
    sk_doc.append('      <Power>%s</Power>' % sk_table.at1.polynom)
    sk_doc.append('      <Potential>%s</Potential>' % sk_table.at1.sigma)
    sk_doc.append('      <Density>%s</Density>' % sk_table.at1.r_dens)
    sk_doc.append('      <Wavefunction>%s</Wavefunction>' %
                  sk_table.at1.r_wf)
    sk_doc.append('      <HubbDerivative>%s</HubbDerivative>' %
               sk_table.at1.u_der)
    sk_doc.append('    </Basis>')
    if (tHomo):
        pass
    else:
        sk_doc.append('    <Basis atom="2">')
        sk_doc.append('      <Shells>%s</Shells>' % sk_table.at2.shells())
        sk_doc.append('      <Exponents>%s</Exponents>' %
                   sk_table.at2.exponents())
        sk_doc.append('      <Power>%s</Power>' % sk_table.at2.polynom)
        sk_doc.append('      <Potential>%s</Potential>' % sk_table.at2.sigma)
        sk_doc.append('      <Density>%s</Density>' % sk_table.at2.r_dens)
        sk_doc.append('      <Wavefunction>%s</Wavefunction>' %
                   sk_table.at2.r_wf)
        sk_doc.append('      <HubbDerivative>%s</HubbDerivative>' %
                      sk_table.at2.u_der)
        sk_doc.append('    </Basis>')
    sk_doc.append('  </SK_table>')
    return sk_doc


def get_rep_doc(e_rep):
    '''Generates the repulsive potential section of the Slater-Koster file
    documentation.'''

    rep_doc = []
    rep_doc.append('  <E_rep>')
    rep_doc.append('    <Storage>%s</Storage>' % e_rep.storage)
    rep_doc.append('    <SCC>%s</SCC>' % e_rep.scc)
    rep_doc.append('    <Full3rd>%s</Full3rd>' % e_rep.thirdorder)
    rep_doc.append('    <Ab_initio>')
    for line in e_rep.abinitio:
        rep_doc.append('      %s' % line)
    rep_doc.append('    </Ab_initio>')
    rep_doc.append('    <Fit_systems>')
    for line in e_rep.fitsystems:
        rep_doc.append('      %s' % line)
    rep_doc.append('    </Fit_systems>')
    rep_doc.append('  </E_rep>')
    return rep_doc


def get_misc_doc(misc):
    '''Generates the miscellaneous section of the Slater-Koster file
    documentation.'''

    misc_doc = []
    misc_doc.append('  <Misc>')
    misc_doc.append('    <Accuracy>')
    for line in misc.accuracy:
        misc_doc.append('      %s' % line)
    misc_doc.append('    </Accuracy>')
    misc_doc.append('    <Failures>')
    for line in misc.failures:
        misc_doc.append('      %s' % line)
    misc_doc.append('    </Failures>')
    misc_doc.append('    <Publications>')
    misc_doc.append('      <Fit>')
    for line in misc.publication:
        misc_doc.append('        %s' % line)
    misc_doc.append('      </Fit>')
    misc_doc.append('    </Publications>')
    misc_doc.append('    <Notes>')
    for line in misc.notes:
        misc_doc.append('        %s' % line)
    misc_doc.append('    </Notes>')
    misc_doc.append('  </Misc>')
    return misc_doc


def write_slko(filename: str, slko_dir: str, out_dir: str, doc: list):
    '''Copies the existing Slater-Koster file and copies it to the output
    destination, adding the documentation string list.'''
    orig_file = '%s/%s' % (slko_dir, filename)
    out_file = '%s/%s' % (out_dir, filename)
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    if (os.path.exists(orig_file)):
        shutil.copy2(orig_file, out_file)
    else:
        print('%s not found. Please check filenames and paths. ' \
              'Skipping...' % orig_file)
        return False
    with open(out_file, 'a') as slko_file:
        for line in doc:
            slko_file.write('%s\n' % line)
    return True


## Main program

def main():
    '''Main program'''

    ## Command line parser
    _paa = 'slko_documentation.py - A program to document DFTB ' \
           'Slater-Koster files. Learn more about DFTB at http://www.dftb.org/'
    parser = argparse.ArgumentParser(description=_paa,
                                     epilog=('Maximilian Kubillus, Feb 2015, '
                                             'maximilian.kubillus@gmail.com'))
    _paa = 'Filename of the program input. Regarding input format' \
           'specification see documentation README file.'
    parser.add_argument('input', metavar='input.cfg', type=str, action='store',
                        nargs='+', help=textwrap.dedent(_paa))
    parser.add_argument('-v', '--version', action='version',
                        version='%(prog)s 0.3')
    args = parser.parse_args()

    try:
        input_file = open(args.input[0], 'r').readlines()
        inputs = InputData(input_file)
        print('Input successfully read. Building documentation.')
    except FileNotFoundError:
        sys.exit('Error: Input file %s not found.' % args.input[0])

    for at1 in inputs.atoms:
        for at2 in inputs.atoms:
            if (at1 == at2):
                tHomo = True
            else:
                tHomo = False
            doc = get_doc(inputs, at1, at2, tHomo)
            filename = '%s-%s.skf' % (at1, at2)
            write_slko(filename, inputs.slko_dir, inputs.out_dir, doc)
    print('Documented Slater-Koster files successfully written to %s.' %
          inputs.out_dir)


if __name__ == '__main__':
    main()
else:
    pass

